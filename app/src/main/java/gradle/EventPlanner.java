package gradle;
import java.util.ArrayList;
import java.util.List;

public class EventPlanner {
  private List<Event> events;

  public EventPlanner() {
    this.events = new ArrayList<>();
  }

  public void addEvent(Event event) {
    events.add(event);
  }

  public void displayEvents() {
    for (Event event : events) {
      System.out.println(event);
    }
  }
  public List<Event> getEvents() {
        return this.events;
    }
}
  

