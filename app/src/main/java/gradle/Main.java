package gradle;
public class Main {
  public static void main(String[] args) {
    EventPlanner eventPlanner = new EventPlanner();
    eventPlanner.addEvent(new Event("Gradle Course Launch", "2023-06-02", "Online"));
    eventPlanner.addEvent(new Event("OpenAI Talk", "2023-06-10", "San Francisco"));
    eventPlanner.displayEvents();
  }
}