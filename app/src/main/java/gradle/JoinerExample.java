package gradle;

import java.util.Arrays;
import com.google.common.base.Joiner;
public class JoinerExample {
    public static void main(String[] args) {
        String joinedString = Joiner.on(", ").join(Arrays.asList("Event Planner", "Awesome Course", "Learning Gradle"));
        System.out.println(joinedString);
    }
}