package gradle;

// import org.testng.annotations.Test;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class EventPlannerTest {
    @Test
    public void testAddEvent() {
        EventPlanner eventPlanner = new EventPlanner();
        Event event = new Event("Gradle Course Launch", "2023-06-02", "Online");
        eventPlanner.addEvent(event);
        assertEquals(1, eventPlanner.getEvents().size());
        assertEquals(event, eventPlanner.getEvents().get(0));
    }

    @Test
    public void testEventCount() throws IOException {
        Properties properties = new Properties();
        properties.load(new FileInputStream("/Users/testvagrant/Documents/Gradle/gradle/wrapper/gradle-wrapper.properties"));
        int eventCount = Integer.parseInt(properties.getProperty("eventCount"));
        EventPlanner eventPlanner = new EventPlanner();

        for (int i = 0; i < eventCount; i++) {
            Event event = new Event("Event " + (i + 1), "2023-06-02", "Online");
            eventPlanner.addEvent(event);
        }

        assertEquals(eventCount, eventPlanner.getEvents().size());
    }

}
